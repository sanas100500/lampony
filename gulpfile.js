var gulp = require('gulp');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var  browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var jshint = require('gulp-jshint');
var babel = require('gulp-babel');
var minifyCSS = require('gulp-minify-css');

gulp.task('autoprefixer', () =>
  gulp.src('app/css/style.css')
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(gulp.dest('app/css/'))
);

gulp.task('sass', function(done) {
  return gulp.src('app/scss/**/[^_]*.scss')
    .pipe(sass().on('error', function(error) {
      done(error);
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(gulp.dest('app/css/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});
gulp.task('pug', function(done) {
  return gulp.src('app/pug/**/[^_]*.pug')
    .pipe(pug({
      pretty: true
    }).on('error', function(error) {
      done(error);
    }))
    .pipe(gulp.dest('app/'))
    .pipe(browserSync.reload({
      stream: true
    }));
});

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'app'
      // index: "blog.html"
    },
  })
});

gulp.task('styles', function() {
  var libs = gulp.src(['app/css/bootstrap.css', 'app/css/jquery.ez-plus.css', 'app/css/swiper.min.css'])
    .pipe(concat('libs.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('dist/css'));
  var main = gulp.src('app/css/style.css')
    .pipe(minifyCSS('style.css'))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('scripts', function() {
  var main = gulp.src(['app/js/filter.js', 'app/js/modal.js', 'app/js/slider.js', 'app/js/main.js'])
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(concat('main.js'))
    .on('error', function(err) {
      gutil.log(gutil.colors.red('[Error]'), err.toString());
    })
    .pipe(uglify())
    .on('error', function(err) {
      gutil.log(gutil.colors.red('[Error]'), err.toString());
    })
    .pipe(gulp.dest('dist/js'));
  var lib = gulp.src(['app/js/jquery.min.js', 'app/js/swiper.jquery.min.js', 'app/js/jquery.ez-plus.js', 'app/js/swiper.min.js'])
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(concat('libs.js'))
    .on('error', function(err) {
      gutil.log(gutil.colors.red('[Error]'), err.toString());
    })
    .pipe(uglify())
    .on('error', function(err) {
      gutil.log(gutil.colors.red('[Error]'), err.toString());
    })
    .pipe(gulp.dest('dist/js'));
});

gulp.task('watch', ['browserSync', 'sass', 'pug'], function() {
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/pug/**/*.pug', ['pug']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/**/*.js', browserSync.reload);
});
