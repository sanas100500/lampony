var range = $("#slider-range").slider({
  range: true,
  min: 0,
  max: 14,
  values: [0, 6],
  slide: function(event, ui) {
    $("#range-min").val(ui.values[0]);
    $("#range-max").val(ui.values[1]);
  }
});
$("#range-min").val(range.slider("values", 0)).change(function() {
  console.log(range.slider("option", "values", [1]));
});
$("#range-max").val(range.slider("values", 1).change(function() {
  console.log(range.slider("option", "values", [2]));
}));
