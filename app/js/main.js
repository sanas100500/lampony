$(document).ready(function() {
  $('.catalog-nav__link').click(function() {
    $('.catalog-nav__link').removeClass('catalog-nav__link_active');
    $('.catalog-nav__item').removeClass('catalog-nav__item_active');
    $(this).parent().addClass('catalog-nav__item_active');
    $(this).addClass('catalog-nav__link_active');
  })

  function radioChange(el) {
    $(el).parent().parent().css({
      backgroundImage: 'url(' + $(el).data('img') + ')'
    });
    $(el).parent().parent().children('.product__wrapper').children('.product__name').text($(el).data('name'));
  }

  function radioChangeInit() {
    $('.colors').each(function(index, el) {
      radioChange($(el).children().first());
    });
    $('.colors__radio').change(function(event) {
      radioChange(this);
    });
  }
  $(window).on('productUpdate', function() {
    galleryThumbs.slidePrev();
    radioChangeInit();
    modalInit();
  });
  modalInit();
  radioChangeInit();
});
