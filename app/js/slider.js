var headerSlider = new Swiper('#header-slider', {
  pagination: '.header-slider__pagination',
  nextButton: '.header-slider__next',
  prevButton: '.header-slider__prev',
  paginationClickable: true,
  autoplay: 5000,
  speed: 1000,
  spaceBetween: 20,
  slidesPerView: 1,
  loop: true
});
var galleryTop = new Swiper('.gallery-top', {
  spaceBetween: 10,
  slidesPerView: 1,
});
var galleryThumbs = new Swiper('.gallery-thumbs', {
  nextButton: '.catalog-slider__arrows_next',
  prevButton: '.catalog-slider__arrows_prev',
  centeredSlides: true,
  touchRatio: 0.2,
  slidesPerView: 4,
  slideToClickedSlide: true,
  breakpoints: {
    768: {
      slidesPerView: 1
    },
    992: {
      slidesPerView: 2
    },
    1200: {
      slidesPerView: 3
    }
  }
});
galleryTop.params.control = galleryThumbs;
galleryThumbs.params.control = galleryTop;
