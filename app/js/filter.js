$(document).ready(function() {
  var topSlider = $('.product'),
    botSlider = $('.catalog-slider__bot');
  var filterParam = {
    collections: [],
    gender: [],
    age: [],
    lvl: [],
    changeCheckbox: function() {
      this.addParam(event.target, this.collections, 'data-filter-collections', 'filterCollections');
      this.addParam(event.target, this.gender, 'data-filter-gender', 'filterGender');
      this.addParam(event.target, this.age, 'data-filter-age', 'filterAge');
      this.addParam(event.target, this.lvl, 'data-filter-lvl', 'filterLvl');
    },
    addParam: function(selector, array, attr, data) {
      if ($(selector).attr(attr) !== undefined) {
        if ($(selector).prop("checked") == true) {
          array.push($(selector).data(data));
        } else {
          var index = array.indexOf($(selector).data(data));
          array.splice(index, 1);
        }
        $(window).trigger('addParam', [attr]);
      }
    }
  };

  var indexProduct = {
    index: {
      gender: [],
      collections: [],
      age: [],
      lvl: []
    },
    unique: [],
    getAllIndex: function() {
      topSlider.each(function(index, el) {
        indexProduct.unique.push(index);
      });
    },
    genderFilter: function() {
      indexProduct.index.gender = [];
      if (filterParam.gender.length != 0) {
        topSlider.each(function(index, el) {
          if ((filterParam.gender.length == 1) && (($(el).attr("data-filter-gender") == filterParam.gender[0]) || ($(el).attr("data-filter-gender") == 'm/f'))) {
            indexProduct.index.gender.push(index);
          } else if ((filterParam.gender.length == 2) && ($(el).attr("data-filter-gender") == 'm/f')) {
            indexProduct.index.gender.push(index);
          }
        });
      }
    },
    collectionsFilter: function() {
      indexProduct.index.collections = [];
      if (filterParam.collections.length != 0) {
        topSlider.each(function(index, el) {
          for (var i = 0; i < filterParam.collections.length; i++) {
            if ($(el).attr("data-filter-collections") == filterParam.collections[i]) {
              indexProduct.index.collections.push(index);
            }
          }
        });
      }
    },
    ageFilter: function() {
      if (filterParam.age.length > 1) filterParam.age.splice(0, 1);
      indexProduct.index.age = [];
      if (filterParam.age.length != 0) {
        topSlider.each(function(index, el) {

          if ($(el).attr("data-filter-age") >= filterParam.age[0]) {
            indexProduct.index.age.push(index);
          }
        });
      }
    },
    lvlFilter: function() {
      indexProduct.index.lvl = [];
      if (filterParam.lvl.length != 0) {
        var newArray = [];
        for (let i = 0; i < filterParam.lvl.length; i++) {
          newArray.push(filterParam.lvl[i].split('-'));
        }
        var range = {
          min: parseInt(newArray[0][0]),
          max: parseInt(newArray[0][1])
        };
        if (newArray.length > 1) {
          for (let i = 0; i < newArray.length; i++) {
            for (let ii = 0; ii < newArray[i].length; ii++) {
              if (parseInt(newArray[i][ii]) < range.min) range.min = parseInt(newArray[i][ii]);
              if (parseInt(newArray[i][ii]) > range.max) range.max = parseInt(newArray[i][ii]);
            }
          }
        }
        topSlider.each(function(index, el) {
          if (($(el).attr("data-filter-lvl") >= range.min) && ($(el).attr("data-filter-lvl") <= range.max)) {
            indexProduct.index.lvl.push(index);
          }
        });
      }
    },
    indexUnique: function() {
      var array = [],
        arrayL,
        arrayAll = [],
        lengthParam = 0;
        indexProduct.unique = [];
      if (filterParam.lvl.length != 0) lengthParam++;
      if (filterParam.collections.length != 0) lengthParam++;
      if (filterParam.gender.length != 0) lengthParam++;
      if (filterParam.age.length != 0) lengthParam++;
      for (var key in indexProduct.index) {
        if (indexProduct.index[key].length != 0) {
          array.push(indexProduct.index[key]);
        }
      }
      arrayL = array.length;
      // console.log(arrayL+ " - "+lengthParam);
      if (arrayL < lengthParam) {
        return;
      }
      if (arrayL == 0) {
        indexProduct.getAllIndex();
        return;
      }
      // console.log(123);
      for (let i = 0; i < arrayL; i++) {
        for (let ii = 0; ii < array[i].length; ii++) {
          arrayAll.push(array[i][ii]);
        }
      }
      if (arrayL > 1) {
        var count = 0;
        for (let i = 0; i < arrayAll.length; i++) {
          for (let ii = 0; ii < arrayAll.length; ii++) {
            if (arrayAll[i] == arrayAll[ii]) {
              count++;
            }
          }
          if (count > arrayL - 1) {
            indexProduct.unique.push(arrayAll[i]);
            arrayAll[i] = null;
          }
          count = 0;
        }
      } else {
        for (let i = 0; i < arrayAll.length; i++) {
          indexProduct.unique.push(arrayAll[i]);
        }
      }
    }
  };
  var filterProducts = {
    wrapperTop: $('.gallery-top .swiper-wrapper'),
    wrapperBot: $('.gallery-thumbs .swiper-wrapper'),
    slideOpen: '<div class="swiper-slide">',
    slideClose: '</div>',
    productsTop: '',
    productsBot: '',
    filter: function() {
      for (let i = 0; i < indexProduct.unique.length; i++) {
        if (i % 4 == 0) {
          this.productsTop += this.slideClose;
          this.productsBot += this.slideClose;
        }
        if (i == 0 || i % 4 == 0) {
          this.productsTop += this.slideOpen;
          this.productsBot += this.slideOpen;
        }
        this.productsTop += $(topSlider[indexProduct.unique[i]])[0].outerHTML;
        this.productsBot += $(botSlider[indexProduct.unique[i]])[0].outerHTML;
      }
      $(this.wrapperTop).append(this.productsTop);
      $(this.wrapperBot).append(this.productsBot);
      this.productsTop = '';
      this.productsBot = '';
    }
  };

  $('.filter .checkbox').change(function(event) {
    filterParam.changeCheckbox();
  });
  $(window).on('addParam', function(event, attr) {
    switch (attr) {
      case 'data-filter-collections':
        indexProduct.collectionsFilter();
        break;
      case 'data-filter-gender':
        indexProduct.genderFilter();
        break;
      case 'data-filter-age':
        indexProduct.ageFilter();
        break;
      case 'data-filter-lvl':
        indexProduct.lvlFilter();
        break;
      default:
    }
    indexProduct.indexUnique();
    galleryTop.removeAllSlides();
    galleryThumbs.removeAllSlides();
    filterProducts.filter();
    galleryTop.update();
    galleryThumbs.update();
    $(window).trigger('productUpdate');
  });

});