function modalInit() {
  $('.product__wrapper').click(function(event) {
    loadDate($(this).parent().data('id')).then(function() {
      $('.overlay').fadeIn(400,
        function() {
          $('.modal')
            .css('display', 'block')
            .animate({
              opacity: 1
            }, 200, function() {
              $('body').css({
                overflow: 'hidden'
              });
              $(window).trigger('modalEndLoad');
            });
        });
    });
  });
  $('.modal__cross, .overlay').click(function() {
    $('.modal').animate({
      opacity: 0
    }, 200, function() {
      $(this).css('display', 'none');
      $('.overlay').fadeOut(400);
    });
    $('body').css({
      overflow: 'visible'
    });
  });
};

function loadDate(rout) {
  // return $.get("./modal_" + rout + ".json", function(data, status) {
    return $.get(rout, function(data, status) {
    appendHeader(data);
    appendGallery(data);
    appendContent(data);
  }, 'json');
}

function appendHeader(data) {
  var header = "<h3 class='modal__name'>" + data.name[0] + "</h3>",
    tags = "<span class='modal__tags'>",
    tagClose = "</span>";
  data.tag.forEach(function (item, i, arr) {
    tags += "<span class='modal__tag'>" + data.tag[i] + tagClose;
  });
  // for (let i = 0; i < data.tag.length; i++) {
  //   tags += "<span class='modal__tag'>" + data.tag[i] + tagClose;
  // }
  tags += tagClose;
  header += tags;
  $('.modal__header').empty();
  $('.modal__header').append(header);
}

function appendGallery(data) {
  var gallery = "",
    gallerySelect = "<img id='zoom_01' class='img-singl__img' src='" + data.smallImg[0] + "' data-zoom-image='" + data.bigImg[0] + "'>",
    video = "";

  for (let i = 0; i < data.smallImg.length; i++) {
    gallery += "<a class='img-col__img' data-image='" + data.mdImg[i] + "' data-zoom-image='" + data.bigImg[i] + "' data-name='" + data.name[i] + "' data-articul='" + data.articul[i] + "' style='background-image:url(" + data.smallImg[i] + ");'></a>"
  }
  if (data.videoName[0] != null) {
    video = "<div class='img-singl__video'>";
    for (let i = 0; i < data.videoName.length; i++) {
      video += "<span class='img-singl__video-block'><a class='img-singl__video-link' href='" + data.videoLink[i] + "' target='_blank'>" + data.videoName[i] + "</a></span>";
    }
    video += "</div>";
  }
  gallerySelect = gallerySelect + video;
  $('#gallery_1').empty();
  $('.img-singl').empty();
  $('#gallery_1').append(gallery);
  $('.img-singl').append(gallerySelect);
}

function appendContent(data) {
  var description = "",
    ul_1 = "<ul class='modal__list modal__list_none'> Описание:<li class='modal__item'>" + data.discription + "</li></ul>",
    ul_2 = "",
    ul_3 = "",
    articul = "<div class='modal__line'></div><span class='modal__list'> Артикул: <span class='articul'>" + data.articul[0] + "</span></span>",
    ulClose = "</ul>";
  if (data.property[0] != null) {
    ul_2 = "<ul class='modal__list modal__list_line'> Характеристики:";
    for (let i = 0; i < data.property.length; i++) {
      ul_2 += "<li class='modal__item'>" + data.property[i] + "</li>";
    }
    ul_2 += ulClose;
  }
  if (data.set[0] != null) {
    ul_3 = "<ul class='modal__list modal__list_standart'> В наборе:";
    for (let i = 0; i < data.set.length; i++) {
      ul_3 += "<li class='modal__item'>" + data.set[i] + "</li>";
    }
    ul_3 += ulClose;
  }
  description = ul_1 + ul_2 + ul_3 + articul;
  $('.modal__description').empty();
  $('.modal__description').append(description);
}

$(window).on('modalEndLoad', function() {
  var arrowTop = $('.img-col__arrow_top'),
    arrowBot = $('.img-col__arrow_bot'),
    img = $('.img-col__img'),
    active = 'img-col__img_active';

  arrowBot.unbind('click');
  arrowTop.unbind('click');
  $(window).unbind('imgSwitch');

  if ($(document).width() > 991) {
    $('#zoom_01').ezPlus({
      lensColour: 'rgba(122, 47, 255, 0.271)',
      lensOpacity: 1,
      cursor: 'pointer',
      zoomWindowWidth: 500,
      zoomWindowHeight: 600,
      scrollZoom: true,
      borderSize: 0,
      gallery: 'gallery_1',
      galleryActiveClass: 'img-col__img_active'
    });
    arrowTop.click(function() {
      $('.img-col__img_active').prev().trigger("click");
    });
    arrowBot.click(function() {
      $('.img-col__img_active').next().trigger("click");
    });

    img.click(function() {
      $('.articul').text($(this).data('articul'));
      $('.modal__name').text($(this).data('name'));
    });

  } else {
    $(window).on('imgSwitch', function() {
      $('#zoom_01').attr('src', $('.img-col__img_active').data('image'));
      $('.articul').text($('.img-col__img_active').data('articul'));
      $('.modal__name').text($('.img-col__img_active').data('name'));
    });
    arrowTop.click(function() {
      $('.img-col__img_active').prev().addClass(active).next().removeClass(active);
      $(window).trigger('imgSwitch');
    });
    arrowBot.click(function() {
      $('.img-col__img_active').next().addClass(active).prev().removeClass(active);
      $(window).trigger('imgSwitch');
    });
    img.click(function() {
      img.removeClass(active);
      $(this).addClass(active);
      $(window).trigger('imgSwitch');
    });
  }
  var img = $('.img-col__img');
  setTimeout(function() {
    $(img[0]).trigger("click");
  }, 100);
});
