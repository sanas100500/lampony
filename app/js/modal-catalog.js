$('#catalog-link').click(function(event) {
  event.preventDefault();
    $('.overlay').fadeIn(400,
      function() {
        $('.modal-catalog')
          .css('display', 'flex')
          .animate({
            opacity: 1
          }, 200, function() {
            $('body').css({
              overflow: 'hidden'
            });
          });
      });
  });
$('.modal-catalog__close, .overlay').click(function() {
  $('.modal-catalog').animate({
    opacity: 0
  }, 200, function() {
    $(this).css('display', 'none');
    $('.overlay').fadeOut(400);
  });
  $('body').css({
    overflow: 'visible'
  });
});
